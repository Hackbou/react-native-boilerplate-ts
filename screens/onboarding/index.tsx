import { View, Text, Button } from 'react-native'
import React from 'react'
import { OnboardingScreenProps } from 'types/screen'

const OnBoardingScreen = ({navigation}: OnboardingScreenProps) => {
  return (
    <View>
      <Text>OnBoardingScreen</Text>
      <Button title='Go to auth' onPress={() => navigation.push("Auth")} />
    </View>
  )
}

export default OnBoardingScreen