import { useSelector } from "react-redux";
import { getUserStatus } from "@stores/features/userSlice";

function useAuthenticated() {
  const isAuth = useSelector(getUserStatus);
  return isAuth;
}

export default useAuthenticated;
