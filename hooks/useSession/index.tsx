import { useContext } from "react";
import { AuthContext } from "@contexts/Auth";

const useSession = () => {
  const authSession = useContext(AuthContext);
  return { ...authSession };
};

export default useSession;
