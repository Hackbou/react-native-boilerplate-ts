import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { RootStackParamList } from "types/screen";
import OnboardingScreen from "@screens/onboarding";
import { StatusBar } from "react-native";
import useAuthenticated from "@hooks/useAuthenticated";
import AppScreen from "./App";
import AuthScreen from "./Auth";

const Stack = createNativeStackNavigator<RootStackParamList>();

function RootNavigation() {
  const isAuth = useAuthenticated();
  console.log({ isAuth });

  return (
    <>
      <StatusBar barStyle={"dark-content"} backgroundColor={"transparent"} />
      <Stack.Navigator
        initialRouteName="Onboarding"
        screenOptions={{ headerShown: false }}
      >
        {isAuth ? (
          <Stack.Screen
            name="App"
            component={AppScreen}
            options={{ headerShown: false }}
          />
        ) : (
          <Stack.Screen
            name="Auth"
            component={AuthScreen}
            options={{ headerShown: false }}
          />
        )}
        <Stack.Screen name="Onboarding" component={OnboardingScreen} />
      </Stack.Navigator>
    </>
  );
}

export default RootNavigation;
