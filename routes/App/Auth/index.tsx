import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { AuthStackParamList } from "types/screen";
import SignUpScreen from "@screens/auth/SignUp";
import SignInScreen from "@screens/auth/SignIn";

const Stack = createNativeStackNavigator<AuthStackParamList>();

const AuthScreen = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="SignUp" component={SignUpScreen} />
      <Stack.Screen name="SignIn" component={SignInScreen} />
    </Stack.Navigator>
  );
};

export default AuthScreen;
