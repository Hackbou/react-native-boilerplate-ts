import { createNativeStackNavigator } from "@react-navigation/native-stack";
import ServiceScreen from "@screens/App/Accueil";
import ArticleScreen from "@screens/App/Accueil/Article";
import { HomeStackParamList } from "types/screen";
import CreneauRoot from "./Creneaux";
import CommandeScreen from "@screens/App/Accueil/Commande";

const Stack = createNativeStackNavigator<HomeStackParamList>();

function HomeRoot() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Accueil" component={ServiceScreen} />
      <Stack.Screen
        name="Creneau"
        component={CreneauRoot}
        options={{ headerShown: false }}
      />
      <Stack.Screen name="Article" component={ArticleScreen} />
      <Stack.Screen name="Commande" component={CommandeScreen} />
    </Stack.Navigator>
  );
}

export default HomeRoot;
