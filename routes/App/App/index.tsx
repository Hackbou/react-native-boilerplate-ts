import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { AppStackParamList } from "types/screen";
import HomeScreen from "@screens/app/Home";
import ProfileScreen from "@screens/app/Profile";

const Stack = createNativeStackNavigator<AppStackParamList>();

const AppScreen = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen name="Profile" component={ProfileScreen} />
    </Stack.Navigator>
  );
};

export default AppScreen;
