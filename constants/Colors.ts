const tintColorLight = "#4BC6AB";
const tintColorDark = "#4BC6AB";

export const theme = {
  base: "#222831",
};

export const DefaultTheme = "#222831";

// const THEMES: ThemeInterface = {
//   colors: {
//     primary: "#246084",
//     secondary: ["#4BC6AB", "#8A8D9F", "#F9F9FF", "#FFFFFF"],
//   },
// };

export default {
  light: {
    text: "#000",
    background: "#fff",
    tint: tintColorLight,
    tint2: "#246084",
    tabIconDefault: "#ccc",
    tabIconSelected: tintColorLight,
  },
  dark: {
    text: "#fff",
    background: "#000",
    tint: tintColorDark,
    tint2: "#246084",
    tabIconDefault: "#ccc",
    tabIconSelected: tintColorDark,
  },
};
