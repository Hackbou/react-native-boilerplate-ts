import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { persistReducer, persistStore } from "redux-persist";
import AsyncStorage from "@react-native-async-storage/async-storage";
import userIdReducer from "../features/userSlice";
import { baseApi } from "./apiMananger";

const persistConfig = {
  key: "fffMobileRoot",
  storage: AsyncStorage,
};

const rootReducer = combineReducers({
  dataPersisted: persistReducer(
    persistConfig,
    combineReducers({
      user: userIdReducer,
    })
  ),
  [baseApi.reducerPath]: baseApi.reducer,
});

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware({
      immutableCheck: false,
      serializableCheck: false,
    }).concat([baseApi.middleware]);
  },
  devTools: true,
});

export const persistor = persistStore(store);
