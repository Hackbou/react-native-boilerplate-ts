import API from "@routes/Api";
import { ApiMananger } from "../app/apiMananger";
import { ISignUp } from "types/index";

const userApi = ApiMananger.injectEndpoints({
  endpoints: (build) => ({
    getUser: build.query({
      query: () => API.USER.GET_USER(),
    }),
    emailVerification: build.query({
      query: (token: string) => API.USER.VERIFICATION_EMAIL(token),
    }),
    loginUser: build.mutation({
      query(body: { username: string; password: string }) {
        return {
          url: API.USER.LOGIN(),
          method: "POST",
          body,
        };
      },
      invalidatesTags: ["Users"],
    }),
    registerUser: build.mutation<any, ISignUp>({
      query(body) {
        return {
          url: API.USER.REGISTER(),
          method: "POST",
          body,
        };
      },
      invalidatesTags: ["Users"],
    }),
    updatePassword: build.mutation({
      query(body: any) {
        return {
          url: "",
          method: "PUT",
          body,
        };
      },
      invalidatesTags: ["Users"],
    }),
  }),
});

export const {
  useLoginUserMutation,
  useRegisterUserMutation,
  useUpdatePasswordMutation,
  useGetUserQuery,
  useEmailVerificationQuery,
} = userApi;
