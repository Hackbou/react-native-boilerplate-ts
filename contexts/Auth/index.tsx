import { ISignIn, ISignUp } from "types/index";
import * as React from "react";
import {
  useLoginUserMutation,
  useRegisterUserMutation,
} from "@stores/features/userApi";
import { useDispatch } from "react-redux";
import { setCredentials } from "@stores/features/userSlice";
import { useNavigation } from "@react-navigation/native";
// import * as SecureStore from "expo-secure-store";

export const AuthContext = React.createContext<{
  signIn: (data: ISignIn) => void;
  signUp: (data: ISignUp) => void;
  signOut: () => void;
  isSignInLoading: boolean;
  isSignUpLoading: boolean;
} | null>(null);

const AuthProvider = ({ children }: { children: React.ReactNode }) => {
  const [SIGN_IN, { isLoading: isSignInLoading }] = useLoginUserMutation();
  const [SIGN_UP, { isLoading: isSignUpLoading }] = useRegisterUserMutation();

  const dispatch = useDispatch();
  const navigation: any = useNavigation();

  const signUp = async (data: ISignUp) => {
    try {
      const res = await SIGN_UP({ ...data });
      console.log({ signUp: res });
    } catch (error) {
      console.log({ error });
    }
  };

  const signIn = async (data: ISignIn) => {
    try {
      const res: any = await SIGN_IN({ ...data });
      if (res?.error) {
        return console.log({ signInError: res?.error });
      }
      console.log({ signIn: res });
      dispatch(setCredentials(res?.data));
      navigation.navigate("App");
    } catch (error) {
      console.log({ error });
    }
  };

  const signOut = () => {};

  return (
    <AuthContext.Provider
      value={{
        signIn,
        signUp,
        signOut,
        isSignInLoading,
        isSignUpLoading,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthProvider;
