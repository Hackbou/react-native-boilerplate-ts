import AuthProvider from "@contexts/Auth";
import { NavigationContainer } from "@react-navigation/native";
import RootNavigation from "@routes/App";
import { persistor, store } from "@stores/app/Store";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";

export default function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NavigationContainer>
          <AuthProvider>
            <RootNavigation />
          </AuthProvider>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
}
