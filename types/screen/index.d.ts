import type { NativeStackScreenProps } from "@react-navigation/native-stack";
import type { BottomTabScreenProps } from "@react-navigation/bottom-tabs";

export type RootStackParamList = {
  App: BottomTabScreenProps;
  Auth: undefined;
  Onboarding: undefined;
};

// export type AppScreenProps = NativeStackScreenProps<RootStackParamList, "Home">;

export type AppScreenProps = NativeStackScreenProps<RootStackParamList, "App">;


/**
 * Auth routes
 */
export type AuthScreenProps = NativeStackScreenProps<
  RootStackParamList,
  "Auth"
>;

export type AuthStackParamList = {
  SignUp: undefined;
  SignIn: undefined;
};

export type SignUpStackProps = NativeStackScreenProps<AuthStackParamList, "SignUp">;

export type SignInStackProps = NativeStackScreenProps<AuthStackParamList, "SignIn">;


/**
 * Onboarding routes
 */
export type OnboardingScreenProps = NativeStackScreenProps<
  RootStackParamList,
  "Onboarding"
>;


/**
 * App routes
 */
export type AppScreenProps = NativeStackScreenProps<
  RootStackParamList,
  "App"
>;

export type AppStackParamList = {
  Home: undefined;
  Profile: undefined;
};

export type HomeStackProps = NativeStackScreenProps<AppStackParamList, "Home">;

export type ProfileStackProps = NativeStackScreenProps<AppStackParamList, "Profile">;