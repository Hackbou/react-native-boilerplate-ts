import React from "react";

export interface ISignIn {
  username: string;
  password: string;
}

export interface ISignUp {
  username: string;
  email: string;
  password: string;
  fullName?: string;
  age?: number;
}

// Themes
export interface ThemeInterface {
  colors: { primary: string; secondary: string[] };
}

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  title: string;
  loading?: boolean;
}

// User
export interface User {
  id?: string;
  username: string;
  email: string;
  password: string;
  age: string;
  fullName: string;
}

// Oder
export interface OderItem {
  id?: string;
  articleId: string;
  quantity: number;
  description?: string;
  // collectionTime?: string;
  // deliveryTime?: string;
}

// export interface Order {
//   articleId?: string;
//   createdAt?: string;
//   id?: string;
//   items?: Order[];
//   orderNumber?: string;
//   status: "PENDING";
//   updatedAt?: string;
//   userId?: number;
// }

export interface OrderType {
  id: string;
  createdAt?: string; //"2023-11-29T12:44:28.048Z"
  updatedAt?: string; // "2023-12-07T01:21:15.316Z"
  articleId?: string;
  userId?: string; // 1
  serviceId?: Number; // 4
  orderNumber?: string; // "ORD-1701261867880-4442"
  quantity: Number;
  description?: string;
  amountArticle?: Number;
  collectionTime?: string; // "20/10/2023 10:30"
  deliveryTime?: string; // "22/10/2023 11:30"
  price?: Number; // 12.332
  status?: string; // "ACCEPTED"
  weight?: string; //10
}

export interface OderItem {
  id?: string;
  articleId: string;
  quantity: number;
  description?: string;
  weight: number;
  article?: any;
  price?: number;
  // collectionTime: Date;
  // deliveryTime: Date;
}

// PROCESSING
// SHIPPED

export enum OrderStatus {
  PROCESSING,
  SHIPPED,
  DELIVERED,
  PENDING,
  ACCEPTED,
  CANCELED,
}

export interface Order {
  articleId?: string;
  createdAt?: string | Date;
  id?: string;
  items?: Order[];
  amountArticle: number;
  orderNumber?: string;
  status:
    | "PENDING"
    | "REJECTED"
    | "ACCEPTED"
    | "DELIVRED"
    | "PROCESSING"
    | "CANCELED"
    | "SHIPPED";
  // status: OrderStatus;
  updatedAt?: string | Date;
  userId?: number;
  price?: number;
  collectionTime?: string;
  deliveryTime?: string;
}

export interface IService {
  id: number;
  name: "LS" | "LSP" | "LSR" | "R" | "S";
  price: number;
}

export interface ICategory {
  id: string;
  name: string;
}

export interface IArticle {
  category: ICategory;
  categoryId: string;
  id: string;
  name: string;
  price: number;
  special: boolean;
  weight: number;
}
